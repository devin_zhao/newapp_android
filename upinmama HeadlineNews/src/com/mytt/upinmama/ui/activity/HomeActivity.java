package com.mytt.upinmama.ui.activity;

import java.util.ArrayList;
import java.util.List;

import org.kymjs.kjframe.ui.BindView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.mytt.upinmama.bean.ChannelBean;
import com.mytt.upinmama.dao.ChannelDao;
import com.mytt.upinmama.headlinenews.R;
import com.mytt.upinmama.ui.adapter.HomeViewAdapter;
import com.mytt.upinmama.ui.view.CircleImageView;
import com.mytt.upinmama.ui.view.LeftMenuView;
import com.viewpagerindicator.TabPageIndicator;

public class HomeActivity extends BaseActivity {
	@BindView(id = R.id.menu_iv, click = true)
	private CircleImageView menu_iv;

	@BindView(id = R.id.home_indicator, click = true)
	private TabPageIndicator home_indicator;

	@BindView(id = R.id.home_pager)
	private ViewPager home_pager;

	@BindView(id = R.id.home_more, click = true)
	private TextView home_more;

	protected SlidingMenu leftMenuView;
	private long mExitTime;

	private HomeViewAdapter viewAdapter;
	private ChannelDao dao;

	private String item[] = { "item0", "item1", "item2", "item3", "item4", "item5" };
	
	private int page;

	@Override
	public void setRootView() {
		setContentView(R.layout.aty_home);

	}

	@Override
	public void initWidget() {
		super.initWidget();
		setTitle("优妈头条");
		leftMenuView = new LeftMenuView(this).initSlidingMenu();
		menu_iv.setVisibility(View.VISIBLE);
		dao = new ChannelDao(this);
	}

	@Override
	protected void onResume() {
		if (0 < dao.getmLists().size()) {
			viewAdapter = new HomeViewAdapter(dao.getmLists());
		} else {
			viewAdapter = new HomeViewAdapter(getHomeMenu());
		}
		home_pager.setAdapter(viewAdapter);
		home_indicator.setViewPager(home_pager);
		home_indicator.notifyDataSetChanged();
		home_indicator.setOnPageChangeListener(pageListener);
		super.onResume();
	}
	
	private OnPageChangeListener pageListener=new OnPageChangeListener() {
		
		@Override
		public void onPageSelected(int arg0) {			
			page=arg0;
		}
		
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub
			
		}
	};

	@SuppressLint("ShowToast")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.menu_iv:
			if (leftMenuView.isMenuShowing()) {
				leftMenuView.showContent();
			} else {
				leftMenuView.showMenu();
			}
			break;

		case R.id.home_more:
			Intent intent = new Intent();
			intent.putExtra("page", page);
			intent.setClass(HomeActivity.this, ChannelActivity.class);
			startActivityForResult(intent, 0x01);
		default:
			break;
		}
	}

	/**
	 * 滑动菜单
	 * 
	 * @return
	 */

	private List<ChannelBean> getHomeMenu() {
		List<ChannelBean> list = new ArrayList<ChannelBean>();
		for (int i = 0; i < item.length; i++) {
			ChannelBean bean = new ChannelBean();
			bean.setTitle(item[i]);
			bean.setImage("http://b.hiphotos.baidu.com/image/pic/item/a044ad345982b2b7993128c533adcbef76099b00.jpg");
			list.add(bean);
		}
		return list;

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0x01) {
			this.page=resultCode;
			home_indicator.setCurrentItem(resultCode);
		}
	}

	@SuppressLint("ShowToast")
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (leftMenuView.isMenuShowing() || leftMenuView.isSecondaryMenuShowing()) {
				leftMenuView.showContent();
			} else {
				if ((System.currentTimeMillis() - mExitTime) > 2000) {
					Toast.makeText(this, "在按一次退出", 0).show();
					mExitTime = System.currentTimeMillis();
				} else {
					finish();
				}
			}
			return true;
		}
		// 拦截MENU按钮点击事件，让他无任何操作
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
