package com.mytt.upinmama.ui.activity;

import org.kymjs.kjframe.KJActivity;

import android.view.View;
import android.widget.TextView;

import com.mytt.upinmama.headlinenews.R;

public class BaseActivity extends KJActivity {
	@Override
	public void setRootView() {

	}

	@Override
	public void initWidget() {
		super.initWidget();
	}
	
	public void setTitle(String title){
		TextView title_tv = (TextView) findViewById(R.id.title_tv);
		title_tv.setText(title);
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
	}
}
