package com.mytt.upinmama.ui.view;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnClosedListener;
import com.mytt.upinmama.bean.LeftMenuBean;
import com.mytt.upinmama.headlinenews.R;
import com.mytt.upinmama.ui.adapter.LeftMenuAdapter;

/**
 * 自定义SlidingMenu 测拉菜单类
 * @author wenlong.lu
 *
 * 2015-6-4 下午3:53:34
 */
public class LeftMenuView implements OnClickListener {
	private final Activity activity;
	SlidingMenu localSlidingMenu;
	
	private String item[] = { "item0", "item1", "item2", "item3", "item4", "item5" };

	
	public LeftMenuView(Activity activity) {
		this.activity = activity;
	}

	public SlidingMenu initSlidingMenu() {
		localSlidingMenu = new SlidingMenu(activity);
		localSlidingMenu.setMode(SlidingMenu.LEFT);// 设置左右滑菜单
		localSlidingMenu.setTouchModeAbove(SlidingMenu.SLIDING_WINDOW);// 设置要使菜单滑动，触碰屏幕的范围
		// localSlidingMenu.setTouchModeBehind(SlidingMenu.SLIDING_CONTENT);//设置了这个会获取不到菜单里面的焦点，所以先注释掉
		localSlidingMenu.setShadowWidthRes(R.dimen.dp_15);// 设置阴影图片的宽度
		localSlidingMenu.setShadowDrawable(R.drawable.leftmenu_shadow);// 设置阴影图片
		localSlidingMenu.setBehindOffsetRes(R.dimen.dp_100);// SlidingMenu划出时主页面显示的剩余宽度
		localSlidingMenu.setFadeDegree(0.35F);// SlidingMenu滑动时的渐变程度
		localSlidingMenu.attachToActivity(activity, SlidingMenu.RIGHT);// 使SlidingMenu附加在Activity右边
		// localSlidingMenu.setBehindWidthRes(R.dimen.left_drawer_avatar_size);//设置SlidingMenu菜单的宽度
		localSlidingMenu.setMenu(R.layout.aty_home_leftmenu);// 设置menu的布局文件
		// localSlidingMenu.toggle();//动态判断自动关闭或开启SlidingMenu
		// localSlidingMenu.setSecondaryMenu(R.layout.profile_drawer_right);
		// localSlidingMenu.setSecondaryShadowDrawable(R.drawable.shadowright);
		localSlidingMenu.setOnOpenedListener(new SlidingMenu.OnOpenedListener() {
			public void onOpened() {

			}
		});
		localSlidingMenu.setOnClosedListener(new OnClosedListener() {

			@Override
			public void onClosed() {
				// TODO Auto-generated method stub

			}
		});
		

		
		LinearLayout leftmenu_headerLayout=(LinearLayout)localSlidingMenu.findViewById(R.id.leftmenu_header);
		TextView leftmenu_setting=(TextView)localSlidingMenu.findViewById(R.id.leftmenu_setting);

		ListView leftmenu_lv=(ListView)localSlidingMenu.findViewById(R.id.leftmenu_lv);
		
		LeftMenuAdapter adapter = new LeftMenuAdapter(activity, getLeftMenu());
		leftmenu_lv.setAdapter(adapter);
		
		return localSlidingMenu;
	}

	@Override
	public void onClick(View v) {
        switch (v.getId()) {
		case R.id.leftmenu_header:
			Toast.makeText(activity, "进入个人中心界面", 0).show();
			break;
		case R.id.leftmenu_setting:
			Toast.makeText(activity, "进入设置页面", 0).show();
			break;
		default:
			break;
		}
	}
	
	/**
	 * 侧滑菜单
	 * 
	 * @return
	 */
	private List<LeftMenuBean> getLeftMenu() {
		List<LeftMenuBean> list = new ArrayList<LeftMenuBean>();
		for (int i = 0; i < item.length; i++) {
			LeftMenuBean bean = new LeftMenuBean();
			bean.setTitle(item[i]);
			bean.setLeftImage(R.drawable.ic_launcher);
			list.add(bean);
		}
		return list;

	}
}
