package com.mytt.upinmama.ui.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mytt.upinmama.bean.LeftMenuBean;
import com.mytt.upinmama.headlinenews.R;
/**
 *  侧滑菜单adapter
 * 
 * @author wenlong.lu
 *
 * 2015-5-29 下午6:35:57
 */
@SuppressLint("InflateParams")
public class LeftMenuAdapter extends BaseAdapter{
	private Context context;
	private List<LeftMenuBean> lists;
	public LeftMenuAdapter(Context context,List<LeftMenuBean> lists){
		this.context=context;
		this.lists=lists;
	}
	
	@Override
	public int getCount() {
		return lists.size();
	}

	@Override
	public Object getItem(int position) {
		return lists.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup group) {
		ViewHolder holder;
		if(view==null){
			holder=new ViewHolder();
			view = LayoutInflater.from(context).inflate(R.layout.aty_home_leftmenu_item, null);
			holder.leftmenu_iv=(ImageView)view.findViewById(R.id.leftmenu_iv);
			holder.leftmenu_tv=(TextView)view.findViewById(R.id.leftmenu_tv);
			view.setTag(holder);
		}else{
			holder=(ViewHolder)view.getTag();			
		}
		final LeftMenuBean bean=(LeftMenuBean)getItem(position);
		holder.leftmenu_tv.setText(bean.getTitle());
		holder.leftmenu_iv.setImageResource(bean.getLeftImage());
		
		view.setOnClickListener(new OnClickListener() {
			
			@SuppressLint("ShowToast")
			@Override
			public void onClick(View arg0) {
				Toast.makeText(context, bean.getTitle(), 0).show();
			}
		});
		return view;
	}
	
	class ViewHolder {
		ImageView leftmenu_iv;
		TextView leftmenu_tv;

	}
}


