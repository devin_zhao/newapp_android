package com.mytt.upinmama.ui.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mytt.upinmama.bean.ChannelBean;
import com.mytt.upinmama.headlinenews.R;

public class ClientChannelAdapter extends BaseAdapter {
	private Activity context;
	// 是否显示底部的ITEM
	private boolean isItemShow = false;
	// 控制的postion
	private int holdPosition;
	// 是否改变
	private boolean isChanged = false;
	// 是否可见
	boolean isVisible = true;
	// 可以拖动的列表（即用户选择的频道列表
	public List<ChannelBean> channelList;
	// 要删除的position
	public int remove_position = -1;
	// 是否可删除
	private boolean isDelete = false;
	//当前页面
	private int page;

	private TextView item_text;
	private TextView delete_iv;

	public ClientChannelAdapter(Activity context, List<ChannelBean> channelList, int page) {
		this.context = context;
		this.channelList = channelList;
		this.page=page;
	}

	@Override
	public int getCount() {
		return channelList == null ? 0 : channelList.size();
	}

	@Override
	public ChannelBean getItem(int position) {
		if (channelList != null && channelList.size() != 0) {
			return channelList.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint({ "ViewHolder", "InflateParams" })
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = LayoutInflater.from(context).inflate(R.layout.aty_home_more_title_item, null);
		RelativeLayout rl_subscribe = (RelativeLayout) view.findViewById(R.id.rl_subscribe);
		item_text = (TextView) view.findViewById(R.id.text_item);
		delete_iv = (TextView) view.findViewById(R.id.delete_iv);
		final ChannelBean channel = getItem(position);
		item_text.setText(channel.getTitle());
		
		if (position == 0 || position == 1) {
			item_text.setEnabled(false);
		} else {
			if (isDelete) {
				delete_iv.setVisibility(View.VISIBLE);
				item_text.setSelected(true);
				Animation shake;
				if (position % 2 == 0) {
					shake = AnimationUtils.loadAnimation(context, R.anim.channel_anim_left);
			
				} else {
					shake = AnimationUtils.loadAnimation(context, R.anim.channel_anim_right);
				}
				shake.reset();
				shake.setFillAfter(true);
				rl_subscribe.startAnimation(shake);
			} else {
				item_text.setSelected(false);
				delete_iv.setVisibility(View.GONE);
			}
		}
		if (isChanged && (position == holdPosition) && !isItemShow) {
			item_text.setText("");
			item_text.setSelected(true);
			item_text.setEnabled(true);
			isChanged = false;
		}
		if (!isVisible && (position == -1 + channelList.size())) {
			item_text.setText("");
			item_text.setSelected(true);
			item_text.setEnabled(true);
		}
		if (remove_position == position) {
			item_text.setText("");
		}

		// 不现实删除的时候 可以点击
		if (!isDelete) {
			view.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					context.setResult(position, context.getIntent());
					context.finish();
				}
			});
		}
		
		if(page==position){
			item_text.setBackgroundColor(android.graphics.Color.parseColor("#00C5CD"));
		}
		
		return view;
	}

	class ViewHolder {
		TextView item_tv;
		TextView edit_iv;

	}

	/** 添加频道列表 */
	public void addItem(ChannelBean channel) {
		channelList.add(channel);
		notifyDataSetChanged();
	}

	/** 拖动变更频道排序 */
	public void exchange(int dragPostion, int dropPostion) {
		holdPosition = dropPostion;
		ChannelBean dragItem = getItem(dragPostion);
		if (dragPostion < dropPostion) {
			channelList.add(dropPostion + 1, dragItem);
			channelList.remove(dragPostion);
		} else {
			channelList.add(dropPostion, dragItem);
			channelList.remove(dragPostion + 1);
		}
		isChanged = true;
		notifyDataSetChanged();
	}

	/** 获取频道列表 */
	public List<ChannelBean> getChannnelList() {
		return channelList;
	}

	/** 设置删除的position */
	public void setRemove(int position) {
		remove_position = position;
		notifyDataSetChanged();
	}

	/** 删除频道列表 */
	public void remove() {
		channelList.remove(remove_position);
		remove_position = -1;
		notifyDataSetChanged();
	}

	/** 设置频道列表 */
	public void setListDate(List<ChannelBean> list) {
		channelList = list;
	}

	/** 获取是否可见 */
	public boolean isVisible() {
		return isVisible;
	}

	/** 设置是否可见 */
	public void setVisible(boolean visible) {
		isVisible = visible;
	}

	/** 显示放下的ITEM */
	public void setShowDropItem(boolean show) {
		isItemShow = show;
	}

	public void notifyDataSetChanged(boolean isDelete) {
		this.isDelete = isDelete;
		ClientChannelAdapter.this.notifyDataSetChanged();
	}
}