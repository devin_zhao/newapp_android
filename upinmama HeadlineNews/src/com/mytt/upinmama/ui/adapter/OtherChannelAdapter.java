package com.mytt.upinmama.ui.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mytt.upinmama.bean.ChannelBean;
import com.mytt.upinmama.headlinenews.R;

@SuppressLint({ "ViewHolder", "InflateParams" })
public class OtherChannelAdapter extends BaseAdapter {
	private Context context;
	public List<ChannelBean> channelList;
	/** 是否可见 */
	boolean isVisible = true;
	/** 要删除的position */
	public int remove_position = -1;

	public OtherChannelAdapter(Context context, List<ChannelBean> channelList) {
		this.context = context;
		this.channelList = channelList;
	}

	@Override
	public int getCount() {
		return channelList == null ? 0 : channelList.size();
	}

	@Override
	public ChannelBean getItem(int position) {
		if (channelList != null && channelList.size() != 0) {
			return channelList.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = LayoutInflater.from(context).inflate(R.layout.aty_home_more_title_item, null);
			holder.item_tv = (TextView) view.findViewById(R.id.text_item);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		ChannelBean channel = getItem(position);
		holder.item_tv.setText(channel.getTitle());
		if (!isVisible && (position == -1 + channelList.size())) {
			holder.item_tv.setText("");
		}
		if (remove_position == position) {
			holder.item_tv.setText("");
		}
		return view;
	}

	class ViewHolder {
		TextView item_tv;

	}

	/** 获取频道列表 */
	public List<ChannelBean> getChannnelList() {
		return channelList;
	}

	/** 添加频道列表 */
	public void addItem(ChannelBean channel) {
		channelList.add(channel);
		notifyDataSetChanged();
	}

	/** 设置删除的position */
	public void setRemove(int position) {
		remove_position = position;
		notifyDataSetChanged();
		// notifyDataSetChanged();
	}

	/** 删除频道列表 */
	public void remove() {
		channelList.remove(remove_position);
		remove_position = -1;
		notifyDataSetChanged();
	}

	/** 设置频道列表 */
	public void setListDate(List<ChannelBean> list) {
		channelList = list;
	}

	/** 获取是否可见 */
	public boolean isVisible() {
		return isVisible;
	}

	/** 设置是否可见 */
	public void setVisible(boolean visible) {
		isVisible = visible;
	}
}