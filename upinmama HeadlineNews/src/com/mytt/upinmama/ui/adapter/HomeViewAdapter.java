package com.mytt.upinmama.ui.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.mytt.upinmama.bean.ChannelBean;
import com.mytt.upinmama.headlinenews.R;

@SuppressLint("UseSparseArrays")
public class HomeViewAdapter extends PagerAdapter {
	private List<ChannelBean> lists;
	private Map<Integer, View> viewCaches = new HashMap<Integer, View>();

	public HomeViewAdapter(List<ChannelBean> lists) {
		this.lists = lists;
	}

	public View instantiateItem(ViewGroup container, int position) {
		if (viewCaches.containsKey(position)) {
			View view = viewCaches.get(position);
			container.addView(view, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			return view;
		}
		Context context = container.getContext();
		LayoutInflater inflater = LayoutInflater.from(context);
		FrameLayout flayout = (FrameLayout) inflater.inflate(R.layout.aty_home_listview, container, false);
		PullToRefreshListView plv = (PullToRefreshListView) flayout.findViewById(R.id.home_plv);
		HomeAdapter adapter = new HomeAdapter(context, lists);
		plv.setAdapter(adapter);
		container.addView(flayout, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		viewCaches.put(position, flayout);
		return flayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return lists.get(position).getTitle();
	}

	@Override
	public int getCount() {
		return lists.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

}
