package com.mytt.upinmama.ui.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mytt.upinmama.bean.ChannelBean;
import com.mytt.upinmama.headlinenews.R;
import com.mytt.upinmama.util.ImageHelper;

/**
 * 首页list
 * 
 * @author wenlong.lu
 * 
 *         2015-5-29 下午6:35:57
 */
@SuppressLint("InflateParams")
public class HomeAdapter extends BaseAdapter {
	private Context context;
	private List<ChannelBean> lists;

	public HomeAdapter(Context context, List<ChannelBean> lists) {
		this.context = context;
		this.lists = lists;
	}

	@Override
	public int getCount() {
		return lists.size();
	}

	@Override
	public Object getItem(int position) {
		return lists.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup group) {
		ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = LayoutInflater.from(context).inflate(R.layout.aty_home_item, null);
			holder.item_iv = (ImageView) view.findViewById(R.id.aty_home_item_iv);
			holder.item_tv = (TextView) view.findViewById(R.id.aty_home_item_tv);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		final ChannelBean bean = (ChannelBean) getItem(position);
		holder.item_tv.setText(bean.getTitle());
		ImageHelper.displayImage(holder.item_iv, bean.getImage());
		return view;
	}

	class ViewHolder {
		ImageView item_iv;
		TextView item_tv;

	}
}
