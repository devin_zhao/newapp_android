package com.mytt.upinmama.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mytt.upinmama.bean.ChannelBean;
import com.mytt.upinmama.db.DatabaseHelper;

public class ChannelDao {
	private DatabaseHelper helper;

	public ChannelDao(Context context) {
		helper = new DatabaseHelper(context);
	}

	/**
	 * 保存用户频道到数据库
	 * 
	 * @param userList
	 */
	public void saveUserChannel(List<ChannelBean> userList) {
		for (int i = 0; i < userList.size(); i++) {
			ChannelBean bean = (ChannelBean) userList.get(i);
			bean.setOrderId(i);
			bean.setSelected(Integer.valueOf(1));
			update(bean);
		}
	}
	
	/**
	 * 保存其他频道到数据库
	 * @param otherList
	 */
	public void saveOtherChannel(List<ChannelBean> otherList) {
		for (int i = 0; i < otherList.size(); i++) {
			ChannelBean bean = (ChannelBean) otherList.get(i);
			bean.setOrderId(i);
			bean.setSelected(Integer.valueOf(0));
			update(bean);
		}
	}
	

	/**
	 * 更新数据
	 * 
	 * @param lists
	 * @return
	 */
	private boolean update(ChannelBean bean) {
		// TODO Auto-generated method stub
		boolean flag = false;
		SQLiteDatabase database = null;
		long id = -1;
		try {
			database = helper.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("title", bean.getTitle());
			values.put("id", bean.getId());
			values.put("orderId", bean.getOrderId());
			values.put("selected", bean.getSelected());
			id = database.insert(DatabaseHelper.TABLE_CHANNEL, null, values);
			flag = (id != -1 ? true : false);
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (database != null) {
				database.close();
			}
		}
		return flag;
	}

	/**
	 * 获取我得数据
	 * 
	 * @return
	 */
	public List<ChannelBean> getmLists() {
		List<ChannelBean> lists = new ArrayList<ChannelBean>();
		SQLiteDatabase db = helper.getReadableDatabase();
		Cursor cursor = null;
		try {
			cursor = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_CHANNEL + " where " + DatabaseHelper.SELECTED + "=?", new String[] { "1" });
			while (cursor.moveToNext()) {
				ChannelBean bean = new ChannelBean();
				bean.setId(cursor.getInt(1));
				bean.setTitle(cursor.getString(2));
				bean.setOrderId(cursor.getInt(3));
				bean.setSelected(cursor.getInt(4));
				lists.add(bean);
			}
		} finally {
			if (cursor != null)
				cursor.close();
			if (db != null)
				db.close();
		}
		return lists;
	}

	/**
	 * 获取其他
	 * 
	 * @return
	 */

	public List<ChannelBean> getoTherLists() {
		List<ChannelBean> lists = new ArrayList<ChannelBean>();
		SQLiteDatabase db = helper.getReadableDatabase();
		Cursor cursor = null;
		try {
			cursor = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_CHANNEL + " where " + DatabaseHelper.SELECTED + "=?", new String[] { "0" });
			while (cursor.moveToNext()) {
				ChannelBean bean = new ChannelBean();
				bean.setId(cursor.getInt(1));
				bean.setTitle(cursor.getString(2));
				bean.setOrderId(cursor.getInt(3));
				bean.setSelected(cursor.getInt(4));
				lists.add(bean);
			}
		} finally {
			if (cursor != null)
				cursor.close();
			if (db != null)
				db.close();
		}
		return lists;
	}

	public void clear() {
		String sql = "DELETE FROM " + DatabaseHelper.TABLE_CHANNEL + ";";
		SQLiteDatabase db = helper.getWritableDatabase();
		db.execSQL(sql);
		revertSeq();
	}

	private void revertSeq() {
		String sql = "update sqlite_sequence set seq=0 where name='" + DatabaseHelper.TABLE_CHANNEL + "'";
		SQLiteDatabase db = helper.getWritableDatabase();
		db.execSQL(sql);
	}

}
