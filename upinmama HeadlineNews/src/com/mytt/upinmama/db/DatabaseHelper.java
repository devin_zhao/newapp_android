package com.mytt.upinmama.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mytt.upinmama.util.Utils;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "upinmama.db";
	public static final String TABLE_CHANNEL = "channel";// 数据表
	public static final String ID = "id";
	public static final String NAME = "title";
	public static final String ORDERID = "orderId";
	public static final String SELECTED = "selected";

	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, Utils.getVersionCode(context));
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.beginTransaction();
		try {
			String sql = "create table if not exists " + TABLE_CHANNEL + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, " + ID + " INTEGER , " + NAME + " TEXT , " + ORDERID + " INTEGER , " + SELECTED + " SELECTED)";
			db.execSQL(sql);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS TABLE_CHANNEL");
		onCreate(db);
	}

}
