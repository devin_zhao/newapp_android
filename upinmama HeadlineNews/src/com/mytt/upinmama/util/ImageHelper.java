package com.mytt.upinmama.util;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.mytt.upinmama.headlinenews.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

/**
 * 图片资源类
 * 
 * @author wenlong.lu
 * 
 *         2015年4月21日下午2:26:23
 */
public class ImageHelper {

	public static DisplayImageOptions options = ImageHelper.getDisplayImageOptions(R.drawable.default_img);
	public static DisplayImageOptions options2 = ImageHelper.getDisplayImageOptions(R.drawable.default_img_s); 

	public static void displayImage(ImageView imageView, String imageUrl) {
		ImageHelper.displayImage(imageView, imageUrl, options);
	}

	public static void displayImage(ImageView imageView, String imageUrl,
			int defaultImage) {
		if (defaultImage == R.drawable.default_img) {
			ImageHelper.displayImage(imageView, imageUrl, options);
		} else if (defaultImage == R.drawable.default_img_s) {
			ImageHelper.displayImage(imageView, imageUrl, options2);
		} else {
			DisplayImageOptions options = ImageHelper.getDisplayImageOptions(defaultImage);
			ImageHelper.displayImage(imageView, imageUrl, options);
		}
	}
	
	public static DisplayImageOptions getDisplayImageOptions(int defaultImage) {
		return new DisplayImageOptions.Builder()
        .showImageOnLoading(defaultImage) //设置图片在下载期间显示的图片
		.showImageForEmptyUri(defaultImage)// 设置图片Uri为空或是错误的时候显示的图片
		.showImageOnFail(defaultImage) // 设置图片加载/解码过程中错误时候显示的图片
		.cacheInMemory(true)// 设置下载的图片是否缓存在内存中
		.cacheOnDisk(true)// 设置下载的图片是否缓存在SD卡中
		.considerExifParams(false) // 是否考虑JPEG图像EXIF参数（旋转，翻转）
		.imageScaleType(ImageScaleType.EXACTLY)// 设置图片以如何的编码方式显示
		.bitmapConfig(Bitmap.Config.RGB_565)// 设置图片的解码类型
		// .decodingOptions(android.graphics.BitmapFactory.Options.class)//设置图片的解码配置
		.delayBeforeLoading(50)//int
		// delayInMillis为你设置的下载前的延迟时间
		// 设置图片加入缓存前，对bitmap进行设置
		// .preProcessor(BitmapProcessor preProcessor)
		// .resetViewBeforeLoading(true)//设置图片在下载前是否重置，复位
		// .displayer(new RoundedBitmapDisplayer(20))//是否设置为圆角，弧度为多少
		.displayer(new FadeInBitmapDisplayer(200))// 是否图片加载好后渐入的动画时间
		.build();// 构建完成
	}

	public static void displayImage(ImageView imageView, String imageUrl,
			DisplayImageOptions options) {
		ImageLoader.getInstance().displayImage(imageUrl, imageView, options);
	}
}
