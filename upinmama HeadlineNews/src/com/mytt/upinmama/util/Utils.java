package com.mytt.upinmama.util;

import android.content.Context;
import android.content.pm.PackageInfo;

public class Utils {

	// 获取版本号
	public static String getVersionName(Context context) {
		try {
			PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return pi.versionName;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// 获取VersionCode
	public static int getVersionCode(Context context) {
		try {
			PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return pi.versionCode;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

}
