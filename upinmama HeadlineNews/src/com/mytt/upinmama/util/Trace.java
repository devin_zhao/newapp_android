package com.mytt.upinmama.util;

import android.util.Log;

/*
 * Log
 */
public class Trace {

    // Constants are the same as in android.util.Log, levels
    public static final int ERROR = 6;
    public static final int WARN = 5;
    public static final int INFO = 4;
    public static final int DEBUG = 3;
    public static final int VERBOSE = 2;

    public static boolean _isLog = true;
    private static String TAG = "upinmama";

    public static void i(String tag, String message) {
        trace(tag, message, INFO);
    }

    public static void w(String tag, String message) {
        trace(tag, message, WARN);
    }

    public static void e(String tag, String message) {
        trace(tag, message, ERROR);
    }

    public static void d(String tag, String message) {
        trace(tag, message, DEBUG);
    }

    public static void i(String message) {
        i(TAG, message);
    }

    public static void w(String message) {
        w(TAG, message);
    }

    public static void e(String message) {
        e(TAG, message);
    }

    public static void d(String message) {
        d(TAG, message);
    }

    public static void trace(String tag, String message, int level) {
        if (_isLog) {
            long sec = (System.currentTimeMillis() / 1000) % 1000;
            StringBuilder b = new StringBuilder("[")
                    .append(Thread.currentThread().getName()).append("] ")
                    .append("@").append(sec).append(" ").append(message);

            Log.println(level, tag, b.toString());
        }
    }
}
