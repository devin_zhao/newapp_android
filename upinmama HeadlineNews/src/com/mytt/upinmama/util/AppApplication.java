package com.mytt.upinmama.util;

import android.app.Application;

import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

public class AppApplication extends Application{
	public void onCreate() {
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		.memoryCacheExtraOptions(480, 320)// max width, max height，即保存的每个缓存文件的最大长宽
		.threadPoolSize(3)// 线程池内加载的数量
		.threadPriority(Thread.NORM_PRIORITY - 2)
		.denyCacheImageMultipleSizesInMemory()
		.memoryCache(new UsingFreqLimitedMemoryCache(5 * 1024 * 1024))// implementation/你可以通过自己的内存缓存实现
		.memoryCacheSize(5 * 1024 * 1024)
		.defaultDisplayImageOptions(ImageHelper.options)
		.imageDownloader(new BaseImageDownloader(this, 5 * 1000, 30 * 1000)) // connectTimeout 5s),readTimeout(30s)超时时间
		.writeDebugLogs() // Remove for release app
		.build();// 开始构建
	     ImageLoader.getInstance().init(config);
	}
	
}
