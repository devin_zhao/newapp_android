package com.mytt.upinmama.bean;

public class LeftMenuBean extends Bean{
	private static final long serialVersionUID = -1850638073374535903L;
	
	private int leftImage;

	public int getLeftImage() {
		return leftImage;
	}

	public void setLeftImage(int leftImage) {
		this.leftImage = leftImage;
	}

}
