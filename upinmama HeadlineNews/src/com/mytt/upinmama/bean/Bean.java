package com.mytt.upinmama.bean;

import java.io.Serializable;

public class Bean implements Serializable {
	private static final long serialVersionUID = 2959848362907682242L;

	private String title;
	private String image;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
